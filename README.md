# Exemplos AMQP

Exemplo de uso do protocolo AMQP testado com o broker Apache ActiveMQ composto por um produtor (thermostat) e um consumidor (air conditioner).
Para o exemplo funcionar, é necessário que o Apache ActiveMQ esteja iniciado e disponível na máquina local.