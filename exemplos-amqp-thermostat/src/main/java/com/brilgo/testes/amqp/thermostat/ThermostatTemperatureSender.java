package com.brilgo.testes.amqp.thermostat;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.qpid.proton.amqp.messaging.AmqpValue;
import org.apache.qpid.proton.message.Message;
import org.apache.qpid.proton.messenger.Messenger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThermostatTemperatureSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThermostatTemperatureSender.class);

	private static final String AMQP_HUB = "amqp://admin:admin@localhost:5672/thermostatTemperature";
	private static final String AMQP_HUB_SUBJECT = "temperature";
	private static final int TEMPERATURE_SCALE = 2;
	private static final RoundingMode TEMPERATURE_ROUDING_MODE = RoundingMode.HALF_EVEN;
	private static final long MEASUREMENT_INTERVAL = 10000;
	
	private Messenger messenger;

	private ThermostatTemperatureSender(String[] args) {
	}

	private void start() throws IOException {
		messenger = Messenger.Factory.create("MessengerThermostat");
		messenger.start();
	}

	private void stop() {
		messenger.stop();
	}

	private void sendTemperature(String body) {
		Message msg = Message.Factory.create();
		msg.setAddress(AMQP_HUB);
		msg.setSubject(AMQP_HUB_SUBJECT);
		msg.setBody(new AmqpValue(body));
		messenger.put(msg);
		messenger.send();
	}

	private BigDecimal getCurrentTempeture() {
		double temperature = Math.random() * 60;
		double temperatureWithSignal = (Math.random() >= 0.5d) ? temperature : - temperature;
		return new BigDecimal(temperatureWithSignal).setScale(TEMPERATURE_SCALE, TEMPERATURE_ROUDING_MODE);
	}
	
	private void run() throws IOException {
		boolean exit = false;
		start();
		
		while (!exit) {
			String currentTemperature = getCurrentTempeture().toString();
			sendTemperature(currentTemperature);
			LOGGER.info("Sent temperature {}", currentTemperature);
			try {
				Thread.sleep(MEASUREMENT_INTERVAL);
			} catch (InterruptedException e) {
				stop();
				LOGGER.error("Program interrupted!");
			}
		}
		stop();
	}

	public static void main(String[] args) throws IOException {
		new ThermostatTemperatureSender(args).run();
	}
}
