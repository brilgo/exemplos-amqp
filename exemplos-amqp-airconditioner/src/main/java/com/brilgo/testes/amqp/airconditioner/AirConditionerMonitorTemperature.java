package com.brilgo.testes.amqp.airconditioner;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.qpid.proton.amqp.messaging.AmqpValue;
import org.apache.qpid.proton.amqp.messaging.ApplicationProperties;
import org.apache.qpid.proton.message.Message;
import org.apache.qpid.proton.messenger.Messenger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AirConditionerMonitorTemperature {

	private static final Logger LOGGER = LoggerFactory.getLogger(AirConditionerMonitorTemperature.class);

	private static final String AMQP_HUB = "amqp://admin:admin@localhost:5672/thermostatTemperature";
	private static final long MONITORING_INTERVAL = 20000;
	private static final int TEMPERATURE_SCALE = 2;
	private static final RoundingMode TEMPERATURE_ROUDING_MODE = RoundingMode.HALF_EVEN;
	private static final String BREAK_LINE_DELIMITER = "\r\n";
	
	private Messenger messenger;

	private AirConditionerMonitorTemperature(String[] args) {
	}

	private void start() throws IOException {
		messenger = Messenger.Factory.create("MessengerAirConditioner");
		messenger.start();
		messenger.subscribe(AMQP_HUB);
	}

	private void stop() {
		messenger.stop();
	}

	private BigDecimal getAverageTempeture() {
		BigDecimal averageTemperature = BigDecimal.ZERO;
        int ct = 0;
    	messenger.recv();
        while (messenger.incoming() > 0) {
            Message msg = messenger.get();
            ++ct;
            logMessageData(ct, msg);
            averageTemperature = averageTemperature.add(readTemperature(msg));
        }
        
        if (!BigDecimal.ZERO.equals(new BigDecimal(ct))) {
        	averageTemperature = averageTemperature.divide(new BigDecimal(ct)).setScale(TEMPERATURE_SCALE, TEMPERATURE_ROUDING_MODE);
        }
        return averageTemperature;
	}

    private BigDecimal readTemperature(Message msg) {
    	return new BigDecimal(((AmqpValue) msg.getBody()).getValue().toString());
	}

	private void logMessageData(int i, Message msg) {
        StringBuilder logMsg = new StringBuilder("message: ");
        logMsg.append(i).append(BREAK_LINE_DELIMITER);
        logMsg.append("Address: ").append(msg.getAddress()).append(BREAK_LINE_DELIMITER);
        logMsg.append("Subject: ").append(msg.getSubject()).append(BREAK_LINE_DELIMITER);
        if (LOGGER.isDebugEnabled()) {
            logMsg.append("Props:     ").append(msg.getProperties()).append(BREAK_LINE_DELIMITER);
            logMsg.append("App Props: ").append(msg.getApplicationProperties()).append(BREAK_LINE_DELIMITER);
            logMsg.append("Msg Anno:  ").append(msg.getMessageAnnotations()).append(BREAK_LINE_DELIMITER);
            logMsg.append("Del Anno:  ").append(msg.getDeliveryAnnotations()).append(BREAK_LINE_DELIMITER);
        } else {
            ApplicationProperties p = msg.getApplicationProperties();
            String s = (p == null) ? "null" : String.valueOf(p.getValue());
            logMsg.append("Headers: ").append(s).append(BREAK_LINE_DELIMITER);
        }
        logMsg.append(msg.getBody()).append(BREAK_LINE_DELIMITER);
        logMsg.append("END").append(BREAK_LINE_DELIMITER);
        LOGGER.info(logMsg.toString());
    }
	
	private void run() throws IOException {
		boolean exit = false;
		start();
		
		while (!exit) {
			BigDecimal currentTemperature = getAverageTempeture();
			LOGGER.info("Calculated average temperature to {}", currentTemperature);
			try {
				Thread.sleep(MONITORING_INTERVAL);
			} catch (InterruptedException e) {
				stop();
				LOGGER.error("Program interrupted!");
			}
		}
		stop();
	}

	public static void main(String[] args) throws IOException {
		new AirConditionerMonitorTemperature(args).run();
	}
}
